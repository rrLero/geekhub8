package org.geekhub.lesson19.repository.licence;

import org.geekhub.lesson19.db.persistence.License;
import org.geekhub.lesson19.repository.GeneralRepository;

public interface LicenseRepository extends GeneralRepository<License, Integer> {
}
